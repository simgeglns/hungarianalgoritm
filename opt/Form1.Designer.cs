﻿namespace opt
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.calisan_sayisi = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gorev_sayisi = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.birinci_calisan = new System.Windows.Forms.TextBox();
            this.ikinci_calisan = new System.Windows.Forms.TextBox();
            this.ucuncu_calisan = new System.Windows.Forms.TextBox();
            this.dorduncu_calisan = new System.Windows.Forms.TextBox();
            this.besinci_calisan = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.birinci_gorev = new System.Windows.Forms.TextBox();
            this.ikinci_gorev = new System.Windows.Forms.TextBox();
            this.ucuncu_gorev = new System.Windows.Forms.TextBox();
            this.dorduncu_gorev = new System.Windows.Forms.TextBox();
            this.besinci_gorev = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.project_name = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(191, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Çalışan Sayısını Seçiniz";
            // 
            // calisan_sayisi
            // 
            this.calisan_sayisi.BackColor = System.Drawing.SystemColors.Control;
            this.calisan_sayisi.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calisan_sayisi.FormattingEnabled = true;
            this.calisan_sayisi.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5"});
            this.calisan_sayisi.Location = new System.Drawing.Point(347, 76);
            this.calisan_sayisi.Name = "calisan_sayisi";
            this.calisan_sayisi.Size = new System.Drawing.Size(140, 25);
            this.calisan_sayisi.TabIndex = 1;
            this.calisan_sayisi.SelectedIndexChanged += new System.EventHandler(this.isci_sayisi_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(191, 261);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "Görev Miktarını Seçiniz";
            // 
            // gorev_sayisi
            // 
            this.gorev_sayisi.BackColor = System.Drawing.SystemColors.Control;
            this.gorev_sayisi.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gorev_sayisi.FormattingEnabled = true;
            this.gorev_sayisi.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5"});
            this.gorev_sayisi.Location = new System.Drawing.Point(347, 258);
            this.gorev_sayisi.Name = "gorev_sayisi";
            this.gorev_sayisi.Size = new System.Drawing.Size(140, 25);
            this.gorev_sayisi.TabIndex = 3;
            this.gorev_sayisi.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button1.CausesValidation = false;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(467, 444);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(55, 55);
            this.button1.TabIndex = 4;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 30);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(145, 155);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // birinci_calisan
            // 
            this.birinci_calisan.BackColor = System.Drawing.SystemColors.Control;
            this.birinci_calisan.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.birinci_calisan.Location = new System.Drawing.Point(186, 143);
            this.birinci_calisan.Name = "birinci_calisan";
            this.birinci_calisan.Size = new System.Drawing.Size(100, 23);
            this.birinci_calisan.TabIndex = 6;
            this.birinci_calisan.Visible = false;
            // 
            // ikinci_calisan
            // 
            this.ikinci_calisan.BackColor = System.Drawing.SystemColors.Control;
            this.ikinci_calisan.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ikinci_calisan.Location = new System.Drawing.Point(299, 143);
            this.ikinci_calisan.Name = "ikinci_calisan";
            this.ikinci_calisan.Size = new System.Drawing.Size(100, 23);
            this.ikinci_calisan.TabIndex = 7;
            this.ikinci_calisan.Visible = false;
            // 
            // ucuncu_calisan
            // 
            this.ucuncu_calisan.BackColor = System.Drawing.SystemColors.Control;
            this.ucuncu_calisan.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ucuncu_calisan.Location = new System.Drawing.Point(415, 143);
            this.ucuncu_calisan.Name = "ucuncu_calisan";
            this.ucuncu_calisan.Size = new System.Drawing.Size(100, 23);
            this.ucuncu_calisan.TabIndex = 8;
            this.ucuncu_calisan.Visible = false;
            // 
            // dorduncu_calisan
            // 
            this.dorduncu_calisan.BackColor = System.Drawing.SystemColors.Control;
            this.dorduncu_calisan.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dorduncu_calisan.Location = new System.Drawing.Point(231, 174);
            this.dorduncu_calisan.Name = "dorduncu_calisan";
            this.dorduncu_calisan.Size = new System.Drawing.Size(100, 23);
            this.dorduncu_calisan.TabIndex = 9;
            this.dorduncu_calisan.Visible = false;
            // 
            // besinci_calisan
            // 
            this.besinci_calisan.BackColor = System.Drawing.SystemColors.Control;
            this.besinci_calisan.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.besinci_calisan.Location = new System.Drawing.Point(369, 174);
            this.besinci_calisan.Name = "besinci_calisan";
            this.besinci_calisan.Size = new System.Drawing.Size(100, 23);
            this.besinci_calisan.TabIndex = 10;
            this.besinci_calisan.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(183, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "İsimleri Giriniz";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic);
            this.label4.Location = new System.Drawing.Point(183, 304);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "Görevleri Giriniz";
            this.label4.Visible = false;
            // 
            // birinci_gorev
            // 
            this.birinci_gorev.BackColor = System.Drawing.SystemColors.Control;
            this.birinci_gorev.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.birinci_gorev.Location = new System.Drawing.Point(186, 329);
            this.birinci_gorev.Name = "birinci_gorev";
            this.birinci_gorev.Size = new System.Drawing.Size(100, 23);
            this.birinci_gorev.TabIndex = 13;
            this.birinci_gorev.Visible = false;
            // 
            // ikinci_gorev
            // 
            this.ikinci_gorev.BackColor = System.Drawing.SystemColors.Control;
            this.ikinci_gorev.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ikinci_gorev.Location = new System.Drawing.Point(299, 329);
            this.ikinci_gorev.Name = "ikinci_gorev";
            this.ikinci_gorev.Size = new System.Drawing.Size(100, 23);
            this.ikinci_gorev.TabIndex = 14;
            this.ikinci_gorev.Visible = false;
            // 
            // ucuncu_gorev
            // 
            this.ucuncu_gorev.BackColor = System.Drawing.SystemColors.Control;
            this.ucuncu_gorev.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ucuncu_gorev.Location = new System.Drawing.Point(415, 329);
            this.ucuncu_gorev.Name = "ucuncu_gorev";
            this.ucuncu_gorev.Size = new System.Drawing.Size(100, 23);
            this.ucuncu_gorev.TabIndex = 15;
            this.ucuncu_gorev.Visible = false;
            // 
            // dorduncu_gorev
            // 
            this.dorduncu_gorev.BackColor = System.Drawing.SystemColors.Control;
            this.dorduncu_gorev.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dorduncu_gorev.Location = new System.Drawing.Point(231, 360);
            this.dorduncu_gorev.Name = "dorduncu_gorev";
            this.dorduncu_gorev.Size = new System.Drawing.Size(100, 23);
            this.dorduncu_gorev.TabIndex = 16;
            this.dorduncu_gorev.Visible = false;
            // 
            // besinci_gorev
            // 
            this.besinci_gorev.BackColor = System.Drawing.SystemColors.Control;
            this.besinci_gorev.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.besinci_gorev.Location = new System.Drawing.Point(369, 360);
            this.besinci_gorev.Name = "besinci_gorev";
            this.besinci_gorev.Size = new System.Drawing.Size(100, 23);
            this.besinci_gorev.TabIndex = 17;
            this.besinci_gorev.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(98)))), ((int)(((byte)(151)))));
            this.label5.Location = new System.Drawing.Point(12, 219);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 19);
            this.label5.TabIndex = 18;
            this.label5.Text = "Proje Adı";
            // 
            // project_name
            // 
            this.project_name.BackColor = System.Drawing.SystemColors.Control;
            this.project_name.Location = new System.Drawing.Point(16, 241);
            this.project_name.Name = "project_name";
            this.project_name.Size = new System.Drawing.Size(100, 25);
            this.project_name.TabIndex = 19;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.project_name);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.besinci_gorev);
            this.Controls.Add(this.dorduncu_gorev);
            this.Controls.Add(this.ucuncu_gorev);
            this.Controls.Add(this.ikinci_gorev);
            this.Controls.Add(this.birinci_gorev);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.besinci_calisan);
            this.Controls.Add(this.dorduncu_calisan);
            this.Controls.Add(this.ucuncu_calisan);
            this.Controls.Add(this.ikinci_calisan);
            this.Controls.Add(this.birinci_calisan);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.gorev_sayisi);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.calisan_sayisi);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(98)))), ((int)(((byte)(151)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Proje Ekibi Planlaması";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox calisan_sayisi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox gorev_sayisi;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox birinci_calisan;
        private System.Windows.Forms.TextBox ikinci_calisan;
        private System.Windows.Forms.TextBox ucuncu_calisan;
        private System.Windows.Forms.TextBox dorduncu_calisan;
        private System.Windows.Forms.TextBox besinci_calisan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox birinci_gorev;
        private System.Windows.Forms.TextBox ikinci_gorev;
        private System.Windows.Forms.TextBox ucuncu_gorev;
        private System.Windows.Forms.TextBox dorduncu_gorev;
        private System.Windows.Forms.TextBox besinci_gorev;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox project_name;
    }
}

