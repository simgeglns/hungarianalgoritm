﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace opt
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
                                            
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            label1.Text = Form1.isci1;
            label2.Text = Form1.isci2;
            label3.Text = Form1.isci3;
            label4.Text = Form1.isci4;
            label5.Text = Form1.isci5;

            //isci ve gorev sayısı değilse yapay satır/sütun atanır
            if (Form1.isci < Form1.gorev)
            {

            }
            if (Form1.isci > Form1.gorev)
            {

            }

            //isci ve gorev sayısı eşitse dizi oluşturulur

            if (Form1.isci == Form1.gorev)
            {
                baslangic_matris.ColumnCount = Form1.gorev;
                baslangic_matris.RowCount = Form1.isci;

                if (Form1.gorev == 2)
                {
                    baslangic_matris.Columns[0].Name = Form1.gorev1;
                    baslangic_matris.Columns[1].Name = Form1.gorev2;

                }

                if (Form1.gorev == 3)
                {
                    baslangic_matris.Columns[0].Name = Form1.gorev1;
                    baslangic_matris.Columns[1].Name = Form1.gorev2;
                    baslangic_matris.Columns[2].Name = Form1.gorev3;

                }
                if (Form1.gorev == 4)
                {
                    baslangic_matris.Columns[0].Name = Form1.gorev1;
                    baslangic_matris.Columns[1].Name = Form1.gorev2;
                    baslangic_matris.Columns[2].Name = Form1.gorev3;
                    baslangic_matris.Columns[3].Name = Form1.gorev4;

                }
                if (Form1.gorev == 5)
                {
                    baslangic_matris.Columns[0].Name = Form1.gorev1;
                    baslangic_matris.Columns[1].Name = Form1.gorev2;
                    baslangic_matris.Columns[2].Name = Form1.gorev3;
                    baslangic_matris.Columns[3].Name = Form1.gorev4;
                    baslangic_matris.Columns[4].Name = Form1.gorev5;

                }
                                                                                 
            }

        }
        public static int[,] dizi = new int[Form1.isci, Form1.gorev];

        private void button1_Click(object sender, EventArgs e)
        {


            for (int i = 0; i < Form1.isci; i++)
            {
                for (int j = 0; j < Form1.gorev; j++)
                {
                    dizi[i, j] = Convert.ToInt32(baslangic_matris.Rows[i].Cells[j].Value);
                }
            }


            //SATIR İNDİRGEME

            for (int i = 0; i < Form1.isci; i++)
            {
                int row_min = Convert.ToInt32(baslangic_matris.Rows[i].Cells[0].Value);

                for (int j = 0; j < Form1.gorev; j++)
                {
                    if(row_min > Convert.ToInt32(baslangic_matris.Rows[i].Cells[j].Value))
                    {
                        row_min = Convert.ToInt32(baslangic_matris.Rows[i].Cells[j].Value);
                    }
                }
                
                for (int k = 0; k < Form1.gorev; k++)
                {
                    dizi[i, k] = dizi[i, k] - row_min;
                }
            }

            //SATIR İNDİRGEME

            Sonuc nextform = new Sonuc();
            nextform.Show();

        }
    }
}
