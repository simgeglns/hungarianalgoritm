﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace opt
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            gorev1 = birinci_gorev.Text;
            gorev2 = ikinci_gorev.Text;
            gorev3 = ucuncu_gorev.Text;
            gorev4 = dorduncu_gorev.Text;
            gorev5 = besinci_gorev.Text;


            isci1 = birinci_calisan.Text;
            isci2 = ikinci_calisan.Text;
            isci3 = ucuncu_calisan.Text;
            isci4 = dorduncu_calisan.Text;
            isci5 = besinci_calisan.Text;

            Form2 nextform = new Form2();

            nextform.Show();
            this.Hide();
        }

        public static int isci,gorev;
        public static string isci1,isci2,isci3,isci4,isci5;
        public static string gorev1, gorev2, gorev3, gorev4, gorev5;


        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            label4.Visible = true;


            if (gorev_sayisi.SelectedItem =="2")
            {
                birinci_gorev.Visible = true;
                ikinci_gorev.Visible = true;
                ucuncu_gorev.Visible = false;
                dorduncu_gorev.Visible = false;
                besinci_gorev.Visible = false;

            }

            if (gorev_sayisi.SelectedItem == "3")
            {
                birinci_gorev.Visible = true;
                ikinci_gorev.Visible = true;
                ucuncu_gorev.Visible = true;
                dorduncu_gorev.Visible = false;
                besinci_gorev.Visible = false;


            }

            if (gorev_sayisi.SelectedItem == "4")
            {
                birinci_gorev.Visible = true;
                ikinci_gorev.Visible = true;
                ucuncu_gorev.Visible = true;
                dorduncu_gorev.Visible = true;
                besinci_gorev.Visible = false;

            }

            if (gorev_sayisi.SelectedItem == "5")
            {
                birinci_gorev.Visible = true;
                ikinci_gorev.Visible = true;
                ucuncu_gorev.Visible = true;
                dorduncu_gorev.Visible = true;
                besinci_gorev.Visible = true;

            }
            gorev = Convert.ToInt32(gorev_sayisi.Text);


        }



        private void isci_sayisi_SelectedIndexChanged(object sender, EventArgs e)
        {
            label3.Visible = true;


            if (calisan_sayisi.SelectedItem == "2")
            {
                birinci_calisan.Visible = true;
                ikinci_calisan.Visible = true;
                ucuncu_calisan.Visible = false;
                dorduncu_calisan.Visible = false;
                besinci_calisan.Visible = false;

            }

            if (calisan_sayisi.SelectedItem == "3")
            {
                birinci_calisan.Visible = true;
                ikinci_calisan.Visible = true;
                ucuncu_calisan.Visible = true;
                dorduncu_calisan.Visible = false;
                besinci_calisan.Visible = false;

            }

            if (calisan_sayisi.SelectedItem == "4")
            {
                birinci_calisan.Visible = true;
                ikinci_calisan.Visible = true;
                ucuncu_calisan.Visible = true;
                dorduncu_calisan.Visible = true;
                besinci_calisan.Visible = false;

            }

            if (calisan_sayisi.SelectedItem == "5")
            {
                birinci_calisan.Visible = true;
                ikinci_calisan.Visible = true;
                ucuncu_calisan.Visible = true;
                dorduncu_calisan.Visible = true;
                besinci_calisan.Visible = true;

            }


            isci = Convert.ToInt32(calisan_sayisi.Text);

        }
    }
}
